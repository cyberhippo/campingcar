"""campingcar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.conf.urls import url


from reservation import views
from reservation.models import *

urlpatterns = [

    path('admin/', admin.site.urls),

    # User Management
    path('accounts/', include('django.contrib.auth.urls')), # new

    # Index - Home
    path('', views.index, name='index'),

    # User profile
    path('profil/', views.profil, name='profil'),

    # Create a new reservation
    path('new/', views.new_reservation, name='new_reservation'),

    # View the whole calendar
    path('view/all/', views.view_all_reservation, name='view_all_reservation'),

    # Successfully added a reservation
    path('new/success/', views.new_reservation_success, name='new_reservation_success'),

    # Unsuccessfully added a reservation
    path('new/failed/', views.new_reservation_failed, name='new_reservation_failed'),

    # Details of a reservation
    path('details/<int:id>', views.reservation_details, name='details'),

    # Calendar view of the events
    path('calendar/', views.calendar, name='calendar'),
]
