from django import forms

from .models import CampingCar, Reservation

from bootstrap_datepicker_plus import DatePickerInput



class add_reservation_form(forms.Form):
    titre       = forms.CharField(max_length = 200)
    description = forms.CharField(max_length = 1000)
    date_debut  = forms.DateField(widget=DatePickerInput(format='%m/%d/%Y'))
    date_fin    = forms.DateField(widget=DatePickerInput(format='%m/%d/%Y'))
    lieu_debut  = forms.CharField(max_length=500)
    lieu_fin    = forms.CharField(max_length=500)
    distance    = forms.IntegerField()

    class Meta:
        model = Reservation
        fields = (  'titre', 'date_debut', 'date_fin',
                    'lieu_debut', 'lieu_fin',
                    'distance', 'description')
