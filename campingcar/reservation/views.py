from django.shortcuts import render, redirect, render_to_response

from django.contrib.auth.decorators import login_required

from reservation.forms import *
from reservation.models import *

from datetime import datetime, time, date

# Create your views here.

def index(request):
    return render(request, 'reservation/index.html')


def login(request):
    return

def calendar(request):
    all_events = Reservation.objects.all()
    event_arr = []

    for i in all_events:
        event_sub_arr = {}

        event_sub_arr['title'] = i.titre + " | " + i.utilisateur

        start_date = datetime.strptime(str(i.date_debut), "%Y-%m-%d").strftime("%Y-%m-%d")
        end_date = datetime.strptime(str(i.date_fin), "%Y-%m-%d").strftime("%Y-%m-%d")

        event_sub_arr['start'] = start_date
        event_sub_arr['end'] = end_date

        event_arr.append(event_sub_arr)

        # return HttpResponse(json.dumps(event_arr))

    return render(request, 'reservation/calendar.html', {'events': event_arr})

@login_required
def new_reservation(request):
    if request.method == "POST":
        # Create a reservation form
        form = add_reservation_form(request.POST or None)

        if form.is_valid():

            # Create the new reservation object
            reservation = Reservation()

            # Get cleaned form content
            reservation.utilisateur = request.user.username
            reservation.titre       = form.cleaned_data['titre']
            reservation.date_debut  = form.cleaned_data['date_debut']
            reservation.date_fin    = form.cleaned_data['date_fin']
            reservation.lieu_debut  = form.cleaned_data['lieu_debut']
            reservation.lieu_fin    = form.cleaned_data['lieu_fin']
            reservation.distance    = form.cleaned_data['distance']
            reservation.description = form.cleaned_data['description']

            # Save the current Rervation
            reservation.save()

            # Pass variables to the validation page
            information = {}
            information["utilisateur"] = request.user.username
            information["titre"]       = form.cleaned_data['titre']
            information["date_debut"]  = form.cleaned_data['date_debut']
            information["date_fin"]    = form.cleaned_data['date_fin']
            information["lieu_debut"]  = form.cleaned_data['lieu_debut']
            information["lieu_fin"]    = form.cleaned_data['lieu_fin']
            information["distance"]    = form.cleaned_data['distance']
            information["description"] = form.cleaned_data['description']

            return render(request, 'reservation/new_reservation_success.html', {'reservation_infos': information})
    else:

        # Create the form object
        form = add_reservation_form()

    return render(request, 'reservation/new_reservation.html', {'form': form})



def view_all_reservation(request):
    reservations = get_all_reservations(request)
    return render(request, 'reservation/all_reservations.html', {'reservations': reservations})



def new_reservation_success(request):
    return render(request, 'reservation/index.html')



def new_reservation_failed(request):
    return render(request, 'reservation/index.html')


def profil(request):
    all_reservations = Reservation.objects.filter(utilisateur=request.user.username).all()
    kilometrage = 0
    for reservation in all_reservations:
        kilometrage += reservation.distance
    return render(request, 'reservation/profile.html', {'kilometrage' : kilometrage, 'reservations' : all_reservations})


@login_required
def reservation_details(request, id):
    reservation_details = Reservation.objects.filter(id=id).last()
    return render(request, 'reservation/reservation_details.html', {'r' : reservation_details})


def get_all_reservations(request):
    return Reservation.objects.filter().all()
