from django.db import models

from django.core.validators import MinValueValidator


##########################
# Create your models here.
##########################

class CampingCar(models.Model):
    nom         = models.CharField(max_length=100)
    marque      = models.CharField(max_length=100)
    kilometrage = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    lieu        = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Reservation(models.Model):
    utilisateur = models.CharField(max_length=100)
    titre       = models.CharField(max_length=200)
    date_debut  = models.DateField()
    date_fin    = models.DateField()
    lieu_debut  = models.CharField(max_length=100)
    lieu_fin    = models.CharField(max_length=100)
    distance    = models.IntegerField(default=0)
    description = models.TextField()
