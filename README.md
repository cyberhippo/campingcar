# Camping Car

## To Do

* Envoie de mails lors d'une réservation
* Possibilité de modifier ses réservations 
* Distance au lieu d'eco-score

## Installation

```bash
git clone git@gitlab.com:cyberhippo/campingcar.git app

virtualenv -p python3 env

source env/bin/activate

cd app

pip install -r requirements.txt

python manage.py makemigrations

python manage.py migrate

python manage.py runserver &
```
